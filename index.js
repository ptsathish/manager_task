const express = require("express");
const app = express();
const mysql= require("mysql");
const http = require('http');
const server = http.createServer(app);
const bodyParser = require('body-parser');
const cors = require('cors');
const port = process.env.PORT || 80;
const path = require('path');
const util = require('util');
app.use(cors());
app.use(bodyParser.json());
app.use(async(req, res, next) => {
    try {
        res.locals.connection =await mysql.createConnection({
            host: '127.0.0.1',
            user: 'root',
            password: 'password',
            database: 'ramco'
        });
        res.locals.connection.connect();
        next()
    } catch (error) {
        console.log(error,"error");
        next()
    }
 
});
app.get("/", async(req, res) =>{ 
    return res.send("api version 1.0") 
  }) 
app.get("/project", async(req, res)=>{ 
    try {
        let {connection} = res.locals;
        const query = util.promisify(connection.query).bind(connection);
         let project = await query('select * from project');
          res.send({message:"success",data:project})  
      } catch (error) {
        console.log(error,"error");
        res.send("Something went Wrong")
    }
});
app.get("/task", async(req, res)=>{ 
    try {
        let {connection} = res.locals;
        const query = util.promisify(connection.query).bind(connection);
         let tasks = await query('select task_id,project_id,task_desc,s.skill_desc from task left join skill s on s.skill_id=task.skill_id');
          res.send({message:"success",data:tasks})  
      } catch (error) {
        console.log(error,"error");
        res.send("Something went Wrong")
    }
});
app.get("/resources", async(req, res)=>{ 
    try {
        let {connection} = res.locals;
        const query = util.promisify(connection.query).bind(connection);
         let resources = await query('select * from resources r inner join resource_skills rs on r.resource_id=rs.resource_id left join skill s on s.skill_id=rs.skill_id');
          res.send({message:"success",data:resources})  
      } catch (error) {
        console.log(error,"error");
        res.send("Something went Wrong")
    }
});
app.get("/resource_availability", async(req, res)=>{ 
    try {
        let {connection} = res.locals;
        const query = util.promisify(connection.query).bind(connection);
         let resource_availability = await query(`select * from resources r left join resource_skills rs on r.resource_id=rs.resource_id left join resource_availability ra on ra.resource_id=r.resource_id left join skill s on s.skill_id=rs.skill_id`);
          res.send({message:"success",data:resource_availability})  
      } catch (error) {
        console.log(error,"error");
        res.send("Something went Wrong")
    }
});
app.get("/available_resource",async(req,res)=>{
    try {
        let {connection} = res.locals;
        let avail_check_query=`SELECT * from (SELECT P.PROJECT_ID, T.TASK_ID, T.TASK_DESC, T.TASK_START_DATE, T.TASK_END_DATE , R.RESOURCE_ID, 
            R.RESOURCE_NAME, RA.AVAILABLE_START_DATE, RA.AVAILABLE_END_DATE,ALLOTED_FLAG
            FROM PROJECT P
            JOIN TASK T
            ON P.PROJECT_ID = T.PROJECT_ID
             JOIN RESOURCE_SKILLS RS
             ON T.SKILL_ID = RS.SKILL_ID
             JOIN RESOURCES R
             ON RS.RESOURCE_ID = R.RESOURCE_ID
             LEFT JOIN RESOURCE_AVAILABILITY RA
             ON RS.RESOURCE_ID = RA.RESOURCE_ID
             AND T.TASK_START_DATE >= RA.AVAILABLE_START_DATE
             AND T.TASK_END_DATE <= RA.AVAILABLE_END_DATE
             AND RA.ALLOTED_FLAG IN (FALSE,NOT NULL)) as availability where  availability.AVAILABLE_START_DATE is Not null`;
        const query = util.promisify(connection.query).bind(connection);
        let available_resource = await query(avail_check_query);
 res.send({message:"success",data:available_resource})  
    } catch (error) {
        
    }
})

server.listen(port,()=>{
    console.log(`server running successfully on ${port}`);
})