//table stuctures

project:
=========

project_id 
project_desc
project_start_date
project_end_date

task:
======
project_id
task_id
task_desc
task_start_date
task_end_date
skill_id -> (Foreign key of resource_skills (and/or) skills table)

resources:
=========
resource_id
resource_name

skills:
========
skill_id
skill_desc

resource_skills:
===============
resource_id
skill_id

resource_availability:
======================
resource_id
available_start_date
available_end_date
alloted_flag 

<!-- #---------------- -->

routes & sample Datas
============================

1.Route: http://127.0.0.1/project

  sample Data:  {
    message: "success",
    data: [
        {
            project_id: 1,
            project_desc: "Test",
            project_start_date: "2020-12-31T18:30:00.000Z",
            project_end_date: "2021-01-09T18:30:00.000Z"
        }       
        ]   
    }

2.Route: http://127.0.0.1/task

  sample Data:  {
        message: "success",
        data: [
            {
            task_id: 1,
            project_id: 1,
            task_desc: "DB Design",
            skill_desc: "Database"
            },
            {
            task_id: 2,
            project_id: 1,
            task_desc: "Infrastructure",
            skill_desc: "Infra & Networking"
            },
            {
            task_id: 3,
            project_id: 1,
            task_desc: "Web Dev",
            skill_desc: "Web Development"
            }
        ]
        }
3.Route: http://127.0.0.1/resource_availability

  sample Data:  {
        message: "success",
        data: [
        {
        resource_id: 1,
        resource_name: "Sundar",
        skill_id: 1,
        available_start_date: "2020-12-29T18:30:00.000Z",
        available_end_date: "2021-01-14T18:30:00.000Z",
        alloted_flag: 0,
        skill_desc: "Database"
        },
        {
        resource_id: 3,
        resource_name: "Revathi",
        skill_id: 2,
        available_start_date: "2021-01-01T18:30:00.000Z",
        available_end_date: "2021-01-09T18:30:00.000Z",
        alloted_flag: 0,
        skill_desc: "Infra & Networking"
        },
        {
        resource_id: 4,
        resource_name: "Sathish",
        skill_id: 3,
        available_start_date: "2021-01-03T18:30:00.000Z",
        available_end_date: "2021-01-08T18:30:00.000Z",
        alloted_flag: 0,
        skill_desc: "Web Development"
        },
        {
        resource_id: null,
        resource_name: "Vernicaa",
        skill_id: 3,
        available_start_date: null,
        available_end_date: null,
        alloted_flag: null,
        skill_desc: "Web Development"
        }
        ]
        }
4.Route: http://127.0.0.1/available_resource
 This is algorgithm that matches the available resource as per project task and 
 returned Data based on the above sample Datas.

 {
        message: "success",
        data: [
        {
        PROJECT_ID: 1,
        TASK_ID: 1,
        TASK_DESC: "DB Design",
        TASK_START_DATE: "2020-12-31T18:30:00.000Z",
        TASK_END_DATE: "2021-01-02T18:30:00.000Z",
        RESOURCE_ID: 1,
        RESOURCE_NAME: "Sundar",
        AVAILABLE_START_DATE: "2020-12-29T18:30:00.000Z",
        AVAILABLE_END_DATE: "2021-01-14T18:30:00.000Z",
        ALLOTED_FLAG: 0
        },
        {
        PROJECT_ID: 1,
        TASK_ID: 2,
        TASK_DESC: "Infrastructure",
        TASK_START_DATE: "2021-01-02T18:30:00.000Z",
        TASK_END_DATE: "2021-01-03T18:30:00.000Z",
        RESOURCE_ID: 3,
        RESOURCE_NAME: "Revathi",
        AVAILABLE_START_DATE: "2021-01-01T18:30:00.000Z",
        AVAILABLE_END_DATE: "2021-01-09T18:30:00.000Z",
        ALLOTED_FLAG: 0
        }
        ]
}
